import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
* @customElement
* @polymer
*/
class VisorMovimientos extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>Sus movimientos</h1>
     <h3>IBAN - {{iban}}</h3>
     <table cellspacing="10" cellpadding="10" border="3" width="900">
      <tr>
        <th style="width:300px">Tipo de Movimiento</th>
        <th style="width:300px">Fecha</th>
        <th style="width:300px">Importe</th>
      </tr>
    </table>

      <dom-repeat items="[[movements]]">
       <template>


         <table cellspacing="10" cellpadding="10" border="1" width="900">
  <!--      <tr>
          <th>Tipo de Movimiento</th>
          <th>Fecha</th>
          <th>Importe</th>
        </tr> -->

          <tr>
            <td style="width:300px">{{item.movement_type}}</td>
            <td style="width:300px">{{item.date}}</td>
            <td style="width:300px">{{item.amount}}</td>
          </tr>

  <!--      <h3>IBAN - {{item.IBAN}}</h3>
        <h3>Tipo de movimiento - {{item.movement_type}}</h3>
        <h3>Fecha - {{item.date}}</h3>
        <h3>Importe - {{item.amount}}</h3> -->
        </table>
       </template>
      </dom-repeat>




     <iron-ajax
       id="getMovements"
       url="http://localhost:3000/apitechu/v2/movements/{{iban}}"
       handle-as="json"
       on-response="showData">
     </iron-ajax>
   `;
 }

 static get properties() {
   return {
     iban : {
       type: String,
       observer: "_ibanChanged"
     }, movements: {
       type: Array
     }
   };
 }

 showData(data) {
   console.log(data.detail.response);
   this.movements = data.detail.response;
 }

 _ibanChanged() {
   this.$.getMovements.generateRequest();
 }
}

window.customElements.define('visor-movimientos', VisorMovimientos);
