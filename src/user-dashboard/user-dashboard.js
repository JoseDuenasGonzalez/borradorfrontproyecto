import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';
import '../visor-cuenta/visor-cuenta.js';
import '@polymer/iron-pages/iron-pages.js'

/**
* @customElement
* @polymer
*/
class UserDashboard extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>LOGIN de Usuario</h1>
     <select value="{{page::change}}">
       <option>Seleccionar componente</option>
       <option value="login">Login Usuario</option>
     </select>



     <iron-pages
        selected="[[page]]"
        attr-for-selected="name"
        fallback-selection="view404"
        role="main">
      <login-usuario name="login" on-loginsucess="processLoginSuccess"></login-usuario>
      <visor-usuario name="usuario" userid={{userid}} on-eventocuentas="consultarCuentas"></visor-usuario>
      <visor-cuenta name="cuenta" userid={{userid}}></visor-cuenta>
    </iron-pages>
    `;
  }

  static get properties() {
    return {

      consultarCuentas2(e) {
        console.log("funcion consultarCuenta");
        this.balance = e.detail.idUser;
        console.log("id:" + this.idUser);
        this.page = "cuenta";
      },

      page: {
        type: String,
      }

    };
  }

  processLoginSuccess(e) {
    console.log("processLoginSuccess");
    console.log(e.detail.idUser);
    this.userid = e.detail.idUser;
    this.page = "usuario";

  }

  consultarCuentas(e) {
    console.log("funcion consultarCuenta");
    console.log(String(e.detail.idUser));
    this.userid = String(e.detail.idUser);
    console.log("id:" + this.iduser);
    this.page = "cuenta";
  }

 }

 window.customElements.define("user-dashboard", UserDashboard);
