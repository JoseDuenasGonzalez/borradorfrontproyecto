import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';
import '../visor-cuenta/visor-cuenta.js';

/**
* @customElement
* @polymer
*/
class UserDashboard extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>LOGIN de Usuario</h1>
     <login-usuario on-loginsucess="processLoginSuccess"></login-usuario>
     <visor-usuario id="visorUsuario"></visor-usuario>
     <visor-cuenta id="visorCuenta"></visor-cuenta>
   `;
 }

 static get properties() {
   return {
   };
 }

 processLoginSuccess(e) {
   console.log("processLoginSuccess");
   console.log(e.detail.idUser);
   this.$.visorUsuario.userid = e.detail.idUser;
   $('#login-usuario').css('display', 'none');
 }
}

window.customElements.define("user-dashboard", UserDashboard);
