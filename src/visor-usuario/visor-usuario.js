import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
* @customElement
* @polymer
*/
class VisorUsuario extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
         /* all: initial; */
         border: solid blue;
       }
       .redbg {
         background-color: red;
       }
       .greenbg {
         background-color: green;
       }
       .bluebg {
         background-color: blue;
       }
       .greybg {
         background-color: grey;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

     <h2>Cliente: [[first_name]] [[last_name]] Nombre de usuario: [[email]] </h2>
  <!--   <visor-cuenta id="visorCuenta"></visor-cuenta> -->
     <button on-click="showAccounts">Ver mis cuentas</button>

     <iron-ajax
       id="getUser"
         url="http://localhost:3000/apitechu/v2/users/{{userid}}"
       handle-as="json"
       on-response="showData"
     >
     </iron-ajax>
   `;
 }
 static get properties() {
   return {
     first_name: {
       type: String
     }, last_name: {
       type: String
     }, email: {
       type: String
     }, userid: {
       type: Number,
       observer: '_useridChanged'
     }
   };
 } // End properties

 showData(data) {
   console.log("showData");
   console.log(data.detail.response);

   this.first_name = data.detail.response.first_name;
   this.last_name = data.detail.response.last_name;
   this.email = data.detail.response.email;

 }

 _useridChanged(newValue, oldValue) {
   this.$.getUser.generateRequest();
 }

 showAccounts(){
   console.log("userid de visor usuario " + this.userid);
   this.dispatchEvent(new CustomEvent("eventocuentas", {"detail":{"userid":this.userid}}));

 }
} // End Class

window.customElements.define('visor-usuario', VisorUsuario);
