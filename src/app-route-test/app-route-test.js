import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js'
import '@polymer/app-route/app-route.js'
/* app-location contiene la ruta que está en el navegador en ese momento */
import '@polymer/app-route/app-location.js'
import '../login-usuario/login-usuario.js'
import '../visor-usuario/visor-usuario.js'
import '../visor-cuenta/visor-cuenta.js'

/**
 * @customElement
 * @polymer
 */
class AppRouteTest extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1 class="jumbotron">Test Iron Pages</h1>
      <app-location route="{{route}}"></app-location>
      <app-route
        route="{{route}}"
        pattern="/:resource"
        data="{{routeData}}"
      >
      </app-route>
      <app-route
        route="{{route}}"
        pattern="/:resource/:id"
        data="{{routeData}}"
      >
      </app-route>
      <iron-pages selected="[[routeData.resource]]" attr-for-selected="component-name">
        <div component-name="login"><login-usuario></login-usuario></div>
        <div component-name="user"><visor-usuario id="[[routeData.id]]"></visor-usuario></div>
        <div component-name="account"><visor-cuenta id="[[routeData.id]]"></visor-cuenta></div>        
      </iron-pages>
    `;
  }
  static get properties() {
    return {
      route: {
        type: Object,
      }, routeData: {
        type: Object
      }
    };
  } // End properties

} // End class

window.customElements.define('app-route-test', AppRouteTest);
