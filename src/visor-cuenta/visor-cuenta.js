import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
* @customElement
* @polymer
*/
class VisorCuenta extends PolymerElement {
 static get template() {
   return html`
     <style>
       :host {
         display: block;
       }
     </style>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <h1>Sus cuentas</h1>

     <div id="cuentas">
      <template
        is="dom-repeat"
        items="{{accounts}}"
        as="account"
        >
        <div>Iban: <span>{{account.IBAN}}</span> Balance: <span>{{account.balance}}</span></div>
      </template>
     </div>

     <iron-ajax
       id="getAccounts"
       url="http://localhost:3000/apitechu/v2/accounts/{{userid}}"
       handle-as="json"
       on-response="showData">
     </iron-ajax>
   `;
 }

 static get properties() {
   return {
     userid : {
       type: Number,
       observer: "_useridChanged"
     }, accounts: {
       type: Array
     }
   };
 }

 showData(data) {
   console.log(data.detail.response);
   this.accounts = data.detail.response;

 }

 _useridChanged() {
   this.$.getAccounts.generateRequest();
 }
}

window.customElements.define('visor-cuenta', VisorCuenta);
